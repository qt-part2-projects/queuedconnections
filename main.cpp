
#include <QApplication>
#include <QMetaType>
#include "data.h"
#include "sender.h"
#include "mythread.h"

int main( int argc, char** argv ) {
    QApplication app( argc, argv );


    Sender sender;
    MyThread thread(&sender);
    thread.start();

    sender.show();
    return app.exec();
}

