
#include "sender.h"
#include "data.h"

Sender::Sender(QWidget *parent) :
    QPushButton("Push me", parent)
{
    connect(this, &Sender::clicked, this, &Sender::sendData);

    m_integer = 42;
    setMinimumSize(200, 30);
}

void Sender::sendData()
{
    Q_EMIT data(m_integer);
}

