
#include <QDebug>
#include <QThread>

#include "receiver.h"
#include "data.h"

Receiver::Receiver(QObject *parent) :
    QObject(parent)
{
}

void Receiver::data(const int &data)
{
    qDebug() << "Receiver says: " << data;

    thread()->quit();
}

